export class LocalStorage {
  /**
   * Класс для упрощения работы с localStorage
   */
  constructor () {
    if (typeof localStorage === 'undefined' || localStorage === null) {
      return
    }

    this.ls = localStorage
  }

  /**
   * Получение поля
   * @param fieldName {string}
   * @returns {any|null}
   */
  get (fieldName) {
    if (typeof localStorage === 'undefined' || localStorage === null) {
      return
    }

    try {
      const value = JSON.parse(this.ls.getItem(fieldName))

      return value.value64
        ? value.value64
        : value
    } catch (e) {
      return this.ls.getItem(fieldName) || null
    }
  }

  /**
   * Запись поля
   * @param fieldName {string}
   * @param value {any}
   */
  set (fieldName, value) {
    if (typeof localStorage === 'undefined' || localStorage === null) {
      return
    }

    if (typeof value === 'string') {
      this.ls.setItem(fieldName, JSON.stringify({ value64: value }))

      return
    }

    this.ls.setItem(fieldName, JSON.stringify(value))
  }

  /**
   * Удаление элемента/элементов
   * @param fields {string|Array}
   */
  remove (fields) {
    if (typeof localStorage === 'undefined' || localStorage === null) {
      return
    }

    const remField = (field) => {
      this.ls.removeItem(field)
    }

    if (Array.isArray(fields)) {
      fields.forEach((item) => {
        remField(item)
      })
    } else {
      remField(fields)
    }
  }

  /**
   * Очистка хранилища
   */
  clear () {
    if (typeof localStorage === 'undefined' || localStorage === null) {
      return
    }

    this.ls.clear()
  }
}

export default new LocalStorage()
