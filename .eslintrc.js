module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parserOptions: {
    parser: '@babel/eslint-parser',
    requireConfigFile: false,
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended',
  ],
  plugins: [
  ],
  // add your custom rules here
  rules: {
    'spaced-comment': 0,
    'comma-dangle': ['error', 'always-multiline'],
    'object-shorthand': ['error', 'always', { avoidExplicitReturnArrows: true }],
    'padding-line-between-statements': [
      'error',
      { blankLine: 'always', prev: '*', next: 'return' },
      { blankLine: 'never', prev: 'empty', next: 'return' },
      { blankLine: 'always', prev: '*', next: 'function' },
      { blankLine: 'never', prev: 'empty', next: 'function' },
    ],
    'lines-between-class-members': ['error', 'always'],
    'arrow-parens': ['error', 'always'],
    'vue/component-name-in-template-casing': ['error', 'kebab-case', { registeredComponentsOnly: false }],
    'no-else-return': 'warn',
  },
  parserOptions: {
    ecmaVersion: 2021,
  },
}
