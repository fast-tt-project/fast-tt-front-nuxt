FROM node:14.17.6

RUN mkdir -p /root/fastttnuxt
COPY . /root/fastttnuxt

WORKDIR /root/fastttnuxt

COPY package*.json /root/fastttnuxt/

RUN npm install

ENV NODE_ENV=production
ENV HOST 0.0.0.0
ENV PORT=5002
EXPOSE 5002

COPY . /root/fastttnuxt

RUN npm run build
CMD npm run start
