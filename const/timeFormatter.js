const weekDays = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота']

export function transformDate (date) {
  const realDate = new Date(date)

  const weekDay = weekDays[realDate.getDay()]
  const day = `${realDate.getDate()}`.padStart(2, '0')
  const month = `${realDate.getMonth() + 1}`.padStart(2, '0')
  const year = realDate.getFullYear()

  return `${day}.${month}.${year} (${weekDay})`
}

export function transformDateTime (dateTime) {
  const realDate = new Date(dateTime.split(' ')[0])

  const weekDay = weekDays[realDate.getDay()]
  const day = `${realDate.getDate()}`.padStart(2, '0')
  const month = `${realDate.getMonth() + 1}`.padStart(2, '0')
  const year = realDate.getFullYear()

  return `${day}.${month}.${year} (${weekDay})`
}
