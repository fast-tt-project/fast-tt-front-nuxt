export const getTgc = (tgc) => `/ftt/get/${tgc}`
export const getTimetable = (params) => `/ftt/get/timetable/${params.optionType}/${params.tgcItem}`
export const postTimetable = (params) => `/ftt/post/timetable/${params.optionType}/${params.tgcItem}`
export const getNews = (page = 1, perPage = 10) => `/ftt/get/news?page=${page}&per_page=${perPage}`
export const getNewsItem = (slug) => `/ftt/get/news/${slug}`
