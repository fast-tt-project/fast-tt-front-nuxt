export const TGC = Object.freeze({
  TEACHERS: 'teachers',
  GROUPS: 'groups',
  CLASSROOMS: 'classrooms',
})

export const TGC_ENUM = Object.freeze({
  [TGC.TEACHERS]: 'Преподаватели',
  [TGC.GROUPS]: 'Группы',
  [TGC.CLASSROOMS]: 'Аудитории',
})

export const MENU_ITEMS_DEFAULT = [
  { id: 'teachers', icon: 'mdi-account', text: TGC_ENUM[TGC.TEACHERS], to: '/timetable/teachers' },
  { id: 'groups', icon: 'mdi-account-multiple', text: TGC_ENUM[TGC.GROUPS], to: '/timetable/groups' },
  { id: 'classrooms', icon: 'mdi-door', text: TGC_ENUM[TGC.CLASSROOMS], to: '/timetable/classrooms' },
]

export const MENU_ITEMS_NAV_DRAWER = [
  { id: 'home', icon: 'mdi-home', text: 'Главная', to: '/' },
  { id: 'news', icon: 'mdi-newspaper-variant-outline', text: 'Новости', to: '/news' },
  ...MENU_ITEMS_DEFAULT,
  { id: 'about', icon: 'mdi-information-outline', text: 'О нас', to: '/about' },
]

export const TIMETABLE = 'timetable'

export const GET_TIMETABLE_LIST_TITLE = (optionType) => `${TGC_ENUM[optionType] || ''} - Fast-tt.ru`
export const GET_TGC_ITEM_TITLE = (tgcItemName) => `${tgcItemName || ''} Расписание - Fast-tt.ru`
